var list = document.getElementById("my-list");

var lis = list.getElementsByTagName("li");

var newItem;

function swap() {
  for (i = lis.length - 1; i >= 0; i--) {
    newItem = lis[i];

    list.removeChild(lis[i]);
    list.appendChild(newItem);
  }
}

// function swap(nodeNumber) {
//   const selection1 = document.querySelector(
//     `li#${nodeNumber}item1:nth-child(${1}`
//   ).innerHTML;
//   const selection2 = document.querySelector(
//     `li#${nodeNumber}item2:nth-child(${2}`
//   ).innerHTML;

//   document.querySelector(`li#${nodeNumber}item1:nth-child(${1}`).innerHTML =
//     selection2;
//   document.querySelector(`li#${nodeNumber}item2:nth-child(${2}`).innerHTML =
//     selection1;
// }

function addPlayers() {
  document.querySelector("#p1").innerHTML = "Sujan";
  document.querySelector("#p2").innerHTML = "Suraj";
}

function setPlayer(player) {
  if (player === 1) {
    const player1 = player === 1 && document.querySelector("#p1").textContent;
    document.querySelector("#name1").setAttribute("value", player1);
  } else {
    const player2 = player === 2 && document.querySelector("#p2").textContent;
    document.querySelector("#name2").setAttribute("value", player2);
  }

  setTimeout(() => {
    document.querySelector("#winner").innerHTML = "Winner is :";
    document.querySelector("#win").innerHTML = "......";
  }, 6000);
}
